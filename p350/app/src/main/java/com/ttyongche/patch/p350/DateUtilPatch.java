package com.ttyongche.patch.p350;

import com.taobao.android.dexposed.DexposedBridge;
import com.taobao.android.dexposed.XC_MethodReplacement;
import com.taobao.android.dexposed.XposedHelpers;
import com.ttyongche.patch.IPatch;
import com.ttyongche.patch.PatchParam;

/**
 * Created by hesc on 15/10/12.
 * <p>由于3.5版本在列表的时间中增加了“准时出发”、“前后15分钟”等标签字样，导致时间显示过程，部分时间显示不出来。</p>
 * <p>本补丁修改订单列表中时间显示，去掉凌晨、上午、中午字样</p>
 * <p>由于OrderItemView会调用DateUtil.getOrderDate()获取时间样式，因此只需要替换getOrderDate的方法实现即可</p>
 */
public class DateUtilPatch implements IPatch {

    private Class<?> loadClass(PatchParam patchParam, String className){
        try {
            return patchParam.context.getClassLoader().loadClass(className);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void handlePatch(PatchParam patchParam) throws Throwable {
        final Class<?> cls = loadClass(patchParam, "com.ttyongche.utils.DateUtil");
        if(cls == null) return;

        DexposedBridge.findAndHookMethod(cls, "getOrderDate", long.class,
                new XC_MethodReplacement() {

                    @Override
                    protected Object replaceHookedMethod(MethodHookParam methodHookParam) throws Throwable {
                        String dateLine = (String) XposedHelpers.callStaticMethod(cls, "getDateLine", new Class[]{long.class}, methodHookParam.args);
                        String timeLine = (String) XposedHelpers.callStaticMethod(cls, "getTimeLine", new Class[]{long.class}, methodHookParam.args);

                        StringBuilder sb = new StringBuilder(dateLine);
                        sb.append(" ").append(timeLine);
                        return sb.toString();
                    }
                }
        );
    }

}
