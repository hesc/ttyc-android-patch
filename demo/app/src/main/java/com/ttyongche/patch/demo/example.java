package com.ttyongche.patch.demo;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;

import com.taobao.android.dexposed.DexposedBridge;
import com.taobao.android.dexposed.XC_MethodHook;
import com.ttyongche.patch.IPatch;
import com.ttyongche.patch.PatchParam;

/**
 * Created by hesc on 15/10/12.
 * <p>在线热补丁示例</p>
 */
public class example implements IPatch {
    @Override
    public void handlePatch(PatchParam patchParam) throws Throwable {
        Class<?> cls = null;
        try {
            cls= patchParam.context.getClassLoader()
                    .loadClass("com.ttyongche.carlife.home.activity.CarlifeHomeActivity");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return;
        }
        DexposedBridge.findAndHookMethod(cls, "onCreate", Bundle.class,
                new XC_MethodHook() {

                    @Override
                    protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                        super.afterHookedMethod(param);
                        Activity activity = (Activity) param.thisObject;
                        Toast.makeText(activity, "欢迎来到天天车管家的世界！", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
